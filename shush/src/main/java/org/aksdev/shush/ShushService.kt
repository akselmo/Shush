package org.aksdev.shush

import android.app.Service
import android.content.Context
import android.content.Intent
import android.hardware.Sensor
import android.hardware.SensorEvent
import android.hardware.SensorEventListener
import android.hardware.SensorManager
import android.media.AudioManager
import android.os.*
import android.widget.TextView
import android.widget.Toast

class ShushService : Service(), SensorEventListener {

    private var isDndEnabled = false
    private var shushToggle = true
    private var isFaceDown = false
    private var isSomethingNear = false

    private lateinit var mSensorManager: SensorManager
    private var mAccelerometer: Sensor? = null
    private var mProximitySensor: Sensor? = null
    private var mVibrator: Vibrator? = null
    private var mVibratorEffect =
        VibrationEffect.createOneShot(200, VibrationEffect.DEFAULT_AMPLITUDE)

    private lateinit var mAudioManager: AudioManager
    private lateinit var sensorText: TextView


    override fun onAccuracyChanged(sensor: Sensor?, accuracy: Int) {
        return
    }

    // TODO: Listen for manual DND presses and disable app if its manually enabled
    // TODO: Create a notification indicating Shush status

    override fun onSensorChanged(event: SensorEvent?) {
        if (event != null) {
            checkAccelerometerValue(event)
            checkProximity(event)
            if (!isSomethingNear || !isFaceDown) {
                isDndEnabled = false
                mAudioManager.ringerMode = AudioManager.RINGER_MODE_NORMAL
            }
        }
    }


    private fun checkAccelerometerValue(event: SensorEvent) {
        if (event.sensor.type == Sensor.TYPE_ACCELEROMETER) {
            isFaceDown = event.values[2] <= -9
        }
    }

    private fun checkProximity(event: SensorEvent) {
        if (event.sensor.type == Sensor.TYPE_PROXIMITY) {
            isSomethingNear = event.values[0] <= 0.5
        }
    }

    private fun toggleDoNotDisturb() {
        val mainHandler = Handler(Looper.getMainLooper())

        mainHandler.post(object : Runnable {
            override fun run() {
                if (isSomethingNear && isFaceDown && !isDndEnabled) {
                    println("Setting DND on!")
                    mAudioManager.ringerMode = AudioManager.RINGER_MODE_SILENT
                    if (Build.VERSION.SDK_INT >= 26) {
                        mVibrator?.vibrate(VibrationEffect.createOneShot(200, VibrationEffect.DEFAULT_AMPLITUDE))
                    } else {
                        mVibrator?.vibrate(200)
                    }
                    isDndEnabled = true
                }
                mainHandler.postDelayed(this, 1000)
            }
        })
    }

    override fun onStartCommand(intent: Intent, flags: Int, startId: Int): Int {
        super.onStartCommand(intent, flags, startId)
        Toast.makeText(
            applicationContext, "Shush has been started.",
            Toast.LENGTH_SHORT
        ).show()
        mSensorManager = getSystemService(Context.SENSOR_SERVICE) as SensorManager
        mAccelerometer = mSensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER)
        mProximitySensor = mSensorManager.getDefaultSensor(Sensor.TYPE_PROXIMITY)
        mAudioManager = getSystemService(Context.AUDIO_SERVICE) as AudioManager
        mSensorManager.registerListener(this, mAccelerometer, SensorManager.SENSOR_DELAY_NORMAL)
        mSensorManager.registerListener(this, mProximitySensor, SensorManager.SENSOR_DELAY_NORMAL)
        mVibrator = getSystemService(Context.VIBRATOR_SERVICE) as Vibrator
        toggleDoNotDisturb()
        return START_STICKY
    }

    override fun onBind(intent: Intent): IBinder? {
        throw UnsupportedOperationException("Not yet implemented")
    }

    override fun onDestroy() {
        super.onDestroy()
        mSensorManager.unregisterListener(this)
        Toast.makeText(
            applicationContext, "Shush has been stopped.",
            Toast.LENGTH_SHORT
        ).show()
    }

}