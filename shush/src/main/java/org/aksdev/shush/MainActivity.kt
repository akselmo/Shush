package org.aksdev.shush

import android.app.NotificationManager
import android.content.Intent
import android.os.Bundle
import android.provider.Settings
import android.widget.Button
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity

class MainActivity : AppCompatActivity() {
    lateinit var mNotificationManager: NotificationManager
    var shushEnabled = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mNotificationManager = this.getSystemService(NOTIFICATION_SERVICE) as NotificationManager
        setContentView(R.layout.activity_main)
        val shushToggleButton: Button = findViewById(R.id.shush_toggle)
        shushToggleButton.setOnClickListener {
            if (checkNotificationAccess()) {
                shushEnabled = !shushEnabled
                if (shushEnabled) {
                    startService(Intent(applicationContext, ShushService::class.java))
                    shushToggleButton.text = "Disable Shush!"
                } else {
                    stopService(Intent(applicationContext, ShushService::class.java))
                    shushToggleButton.text = "Enable Shush!"
                }
            }
        }

    }

    fun checkNotificationAccess(): Boolean {
        if (!mNotificationManager.isNotificationPolicyAccessGranted) {
            Toast.makeText(
                applicationContext, "Please grant Do Not Disturb access for Shush",
                Toast.LENGTH_LONG
            ).show()
            val intent = Intent(Settings.ACTION_NOTIFICATION_POLICY_ACCESS_SETTINGS)
            startActivity(intent)
        }
        return mNotificationManager.isNotificationPolicyAccessGranted
    }

}